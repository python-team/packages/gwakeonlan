# gWakeOnLAN
# Wake up your machines using Wake on LAN.
# Copyright (C) 2009-2022 Fabio Castelli (Muflone) <muflone(at)muflone.com>
# Website: https://www.muflone.com/gwakeonlan/
# This file is distributed under the same license of gWakeOnLAN.
# Russian translation for gWakeOnLAN
#
# Translators:
# Сергей Богатов <hsh(at)runtu.org>, 2010,2015,2020
msgid ""
msgstr ""
"Project-Id-Version: gWakeOnLAN\n"
"Report-Msgid-Bugs-To: https://github.com/muflone/gwakeonlan/issues\n"
"POT-Creation-Date: 2022-03-20 01:19+0100\n"
"PO-Revision-Date: 2020-02-13 13:31+0000\n"
"Last-Translator: Сергей Богатов <hsh(at)runtu.org>\n"
"Language-Team: Russian (https://www.transifex.com/p/gwakeonlan/)\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);\n"

#: gwakeonlan/ui/about.py:59
#, python-brace-format
msgid "Version {VERSION}"
msgstr ""

#: gwakeonlan/ui/about.py:62
msgid "Wake up your machines using Wake on LAN."
msgstr ""

#: gwakeonlan/ui/about.py:71
msgid "Contributors:"
msgstr "Авторы:"

#: gwakeonlan/ui/arpcache.py:43
msgid "Pick a host from the ARP cache"
msgstr "Выбор хоста из кэша ARP"

#: gwakeonlan/ui/detail.py:58 ui/main.ui:59 ui/shortcuts.ui:57
msgid "Edit machine"
msgstr "Редактировать"

#: gwakeonlan/ui/detail.py:60 ui/main.ui:51 ui/shortcuts.ui:50
msgid "Add machine"
msgstr "Добавить"

#: gwakeonlan/ui/detail.py:68
msgid "Missing machine name"
msgstr "Не задано имя компьютера"

#: gwakeonlan/ui/detail.py:72
msgid "Invalid MAC address"
msgstr "Неверный MAC адрес"

#: gwakeonlan/ui/detail.py:76
msgid "Invalid destination host"
msgstr "Неверное расположение"

#: gwakeonlan/ui/main.py:243
msgid "Are you sure you want to remove the selected machine?"
msgstr "Удалить выбранный компьютер из списка?"

#: gwakeonlan/ui/main.py:244 ui/main.ui:67 ui/shortcuts.ui:64
msgid "Delete machine"
msgstr "Удалить"

#: ui/arpcache.ui:101
msgid "IP Address"
msgstr "IP Адрес"

#: ui/arpcache.ui:113
msgid "Host Name"
msgstr "Имя компьютера"

#: ui/arpcache.ui:125
msgid "MAC Address"
msgstr "MAC Адрес"

#: ui/detail.ui:29
msgid "<b><span foreground=\"red\">ERROR</span></b>"
msgstr "<b><span foreground=\"red\">ОШИБКА</span></b>"

#: ui/detail.ui:91
msgid "<big><b>Insert the machine name and its MAC address</b></big>"
msgstr "<big><b>Введите имя и MAC адрес компьютера</b></big>"

#: ui/detail.ui:120
msgid "_Machine name:"
msgstr "_Имя компьютера:"

#: ui/detail.ui:148
msgid "MAC _Address:"
msgstr "MAC _адрес:"

#: ui/detail.ui:162
msgid "_UDP port number:"
msgstr "_Номер порта UDP:"

#: ui/detail.ui:191
msgid "Request type:"
msgstr "Тип запроса:"

#: ui/detail.ui:201
msgid "_Local (broadcast)"
msgstr "_Локальная сеть"

#: ui/detail.ui:218
msgid "_Internet"
msgstr "_Интернет"

#: ui/detail.ui:240
msgid "_Destination host:"
msgstr "_Расположение компьютера:"

#: ui/main.ui:33
msgid "Options"
msgstr ""

#: ui/main.ui:42 ui/main.ui:210 ui/shortcuts.ui:71
msgid "Turn on"
msgstr "Включить"

#: ui/main.ui:75 ui/main.ui:111
msgid "Detect from ARP cache"
msgstr "Найти в ARP кэше"

#: ui/main.ui:81 ui/main.ui:119
msgid "Import from ethers file"
msgstr "Импорт из файла /etc/ethers"

#: ui/main.ui:97 ui/main.ui:148 ui/shortcuts.ui:85
msgid "Deselect All"
msgstr ""

#: ui/main.ui:126
msgid "Select machines"
msgstr ""

#: ui/main.ui:333
msgid "Machine name"
msgstr "Имя компьютера"

#: ui/main.ui:346
msgid "MAC address"
msgstr "MAC адрес"

#: ui/main.ui:359
msgid "Request type"
msgstr "Тип запроса"

#: ui/main.ui:372
msgid "Destination"
msgstr "Расположение"

#: ui/main.ui:385
msgid "Port NR"
msgstr "Порт"

#: ui/shortcuts.ui:21
msgid "About gWakeOnLAN"
msgstr "Справка gWakeOnLAN"

#: ui/shortcuts.ui:28
msgid "Show the shortcuts dialog"
msgstr ""

#: ui/shortcuts.ui:35
msgid "Quit"
msgstr ""

#: ui/shortcuts.ui:45
msgid "Machines"
msgstr ""
